<?php get_header(); ?>

<section>
	<?php if ($THEME_OPTIONS['banner']): ?>
	<div class="banner">
		<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	</div>
	<?php endif ?>
</section>

<section id="pagecontent" class="pagebg">
	<div class="gallery_panel_heading"><span><?php the_title(); ?></span></div>
	<div class="container ">
		<div class="whiteborder">
		<div class="row">
			<?php 
	            $pageimg_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); 
	            $page_image = aq_resize($pageimg_url[0], 700, 500, true, true, true);
	        ?>
	        <?php if ($page_image): ?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
					<img src="<?php echo $page_image; ?>">
			</div>
	        <?php endif ?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<?php echo apply_filters('the_content',$post->post_content); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>