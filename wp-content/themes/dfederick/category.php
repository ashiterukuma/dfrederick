<?php get_header(); ?>

<section>
	<?php if ($THEME_OPTIONS['banner']): ?>
	<div class="banner">
		<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	</div>
	<?php endif ?>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php echo apply_filters('the_content',$post->post_content); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>