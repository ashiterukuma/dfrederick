<?php
define('TEMPLATE_URL', get_stylesheet_directory_uri());
define('ASSET_URL', TEMPLATE_URL . '/assets/');
require_once('includes/aq_resizer.php');


define('DFRE_PAGE_HOME', 4);
define('DFRE_PAGE_ABOUT', 9);
define('DFRE_PAGE_INFRA', 17);
define('DFRE_PAGE_CLIENT', 26);
define('DFRE_PAGE_GALLERY', 19);
define('DFRE_TYPE_PRODUCT', 'product');
define('DFRE_TERM_PRODUCT', 'clothing');
define('DFRE_TERM_DEMO', 'demo-clothing');
define('DFRE_TERM_EXPORT', 'export-clothing');
define('DFRE_TERM_TIESTO', 'tiesto-clothing');



#######################################################
/* * ********* CSS merge and minify process *********** */
#######################################################

if ($_SERVER['HTTP_HOST'] == 'dfrederick.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
    require( 'includes/class.magic-min.php' );
    $minified = new Minifier(
            array(
        'echo' => false
            )
    );
    //exclude example
    $css_exclude = array(
            //TEMPLATEPATH . '/assets/css/bootstrap.css', 
            //TEMPLATEPATH . '/assets/css/flexslider.css'
    );
    //order example
    $css_order = array(
            //TEMPLATEPATH . '/assets/css/reset.css',     
    );

    $minified->merge(TEMPLATEPATH . '/assets/css/combine.min.css', TEMPLATEPATH . '/assets/css/combine', 'css', $css_exclude, $css_order);
}

function the_template_url() {
    echo TEMPLATE_URL;
}

################################################################################
// Enqueue Scripts
################################################################################

function init_scripts() {
    wp_deregister_script('wp-embed');
    wp_deregister_script('jquery');
    wp_deregister_script('comment-reply');
}

function add_scripts() {
    $js_path = ASSET_URL . 'js';
    $css_path = ASSET_URL . 'css';
    if ($_SERVER['HTTP_HOST'] == 'dfrederick.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
        $js_libs = array(
           array(
                'name' => 'jquery',
                'src' => $js_path . '/jquery.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'jquery.min',
                'src' => $js_path . '/jquery.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'jquery-latest.min',
                'src' => $js_path . '/jquery-latest.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'bootstrap',
                'src' => $js_path . '/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'bootstrap-hover-dropdown',
                'src' => $js_path . '/bootstrap-hover-dropdown.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'prettyphoto',
                'src' => $js_path . '/jquery.prettyphoto.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'google map',
                'src' => '//maps.googleapis.com/maps/api/js?key=AIzaSyDEoRTCGDs5OTUY8jlvy9hCkFV1tUP65cY',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),      
            array(
                'name' => 'nivo slider',
                'src' => $js_path . '/jquery.nivo.slider.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'plugins',
                'src' => $js_path . '/plugins.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name'  => 'bootstrap',
                'src'   => $css_path . '/bootstrap.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name' => 'font-awesomes',
                'src' => $css_path . '/font-awesome.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),            
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
        );
    } else {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => $js_path . '/jquery.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'jquery.min',
                'src' => $js_path . '/jquery.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'jquery-latest.min',
                'src' => $js_path . '/jquery-latest.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'bootstrap',
                'src' => $js_path . '/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'bootstrap-hover-dropdown',
                'src' => $js_path . '/bootstrap-hover-dropdown.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'prettyphoto',
                'src' => $js_path . '/jquery.prettyphoto.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'google map',
                'src' => '//maps.googleapis.com/maps/api/js?key=AIzaSyDEoRTCGDs5OTUY8jlvy9hCkFV1tUP65cY',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),      
            array(
                'name' => 'nivo slider',
                'src' => $js_path . '/jquery.nivo.slider.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'plugins',
                'src' => $js_path . '/plugins.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name'  => 'bootstrap',
                'src'   => $css_path . '/bootstrap.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name' => 'font-awesomes',
                'src' => $css_path . '/font-awesome.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),            
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
        );
    }
    foreach ($js_libs as $lib) {
        wp_enqueue_script($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['is_footer']);
    }
    foreach ($css_libs as $lib) {
        wp_enqueue_style($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['media']);
    }
}

function my_deregister_scripts() {
    global $post_type;

    if (!is_front_page() && !is_home()) {
        
    }
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

function my_deregister_styles() {
    global $post_type;
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

if (!is_admin())
    add_action('init', 'init_scripts', 10);
add_action('wp_enqueue_scripts', 'add_scripts', 10);
add_action('wp_enqueue_scripts', 'my_deregister_scripts', 100);
add_action('wp_enqueue_scripts', 'my_deregister_styles', 100);



################################################################################
// Add theme support
################################################################################

add_theme_support('automatic-feed-links');
add_theme_support('nav-menus');
add_post_type_support('page', 'excerpt');
add_theme_support('post-thumbnails', array('post', 'page', 'product'));

if (function_exists('register_nav_menus')) {
    register_nav_menus(
            array(
                'main' => 'Main',
                'helpful' => 'Helpful Info',
                'privacy' => 'Privacy Info',
            )
    );
}

################################################################################
// Add theme sidebars
################################################################################

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('Main - Sidebar'),
        'id' => 'main-sidebar-widget-area',
        'description' => 'Widgets in this area will be shown on the right sidebar of default page',
        'before_widget' => '<aside class="widget">',
        'after_widget' => '</aside>',
        'before_title' => '',
        'after_title' => '',
    ));
//    register_sidebar(array(
//        'name' => __('Page - Sidebar'),
//        'id' => 'page-widget-area',
//        'description' => 'Widgets in this area will be shown on the right sidebar of pages',
//        'before_widget' => '<aside class="widget">',
//        'after_widget' => '</aside>',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Newsletter - Sidebar'),
//        'id' => 'footer-newsletter-widget-area',
//        'description' => 'Widgets in this area will be shown on the top of footer',
//        'before_widget' => '<div class="row" id="newsletter-form-container">',
//        'after_widget' => '</div>',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Left - Sidebar'),
//        'id' => 'footer-left-widget-area',
//        'description' => 'Widgets in this area will be shown on the left-hand side of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Middle - Sidebar'),
//        'id' => 'footer-middle-widget-area',
//        'description' => 'Widgets in this area will be shown in the middle of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Right - Sidebar'),
//        'id' => 'footer-right-widget-area',
//        'description' => 'Widgets in this area will be shown on the right-hand side of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
}





################################################################################
// Comment formatting
################################################################################

function theme_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li>
        <article <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
            <header class="comment-author vcard">
    <?php echo get_avatar($comment, $size = '48', $default = '<path_to_url>'); ?>
    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
                <time><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a></time>
                <?php edit_comment_link(__('(Edit)'), '  ', '') ?>
            </header>
                <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em>
                <br />
            <?php endif; ?>

    <?php comment_text() ?>

            <nav>
            <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </nav>
        </article>
        <!-- </li> is added by wordpress automatically -->
                <?php
            }
            


    /* ======================
     * ajax sendmail function
     * ======================
     */

    function ajax_sendmail_func() {

        GLOBAL $THEME_OPTIONS;
        if ($_POST['form_type'] == 'enquirydata') {

            $to = $THEME_OPTIONS['info_email'];

            $name = $_POST['name'];
            $from = $_POST['email'];
            $phone = $_POST['ph'];
            $message = $_POST['message'];

            $subject = "Contact Form from D.Frederick Int'l Co.,Ltd Website";
            
            $personal_info =  " Fullname        : " . $name . "<br/>
                                Email           : ". $from ."<br>
                                Contact Number  : " . $phone . "<br/>
                                Message         : " . $message ."<br>";
                                        
            $body = "Hi! you have new enquiry from " . $name . "<br /><br/> Customer Information <br/><br/>" . $personal_info;
            }
    

        $content .= "<html>\n";
        $content .= "<body style=\"font-family:Verdana, Verdana, Geneva, sans-serif; font-size:12px; color:#666666;\">\n";
        $body = html_entity_decode($body);
        $content .= $body;
        $content .= "\n\n";
        $content .= "</body>\n";
        $content .= "</html>\n";

        $headers .= 'From: D.Frederick Int\'l Co.,Ltd <noreply@d.frederick.com' . "\r\n";
        $headers .= 'Reply-To:' . $name . '<' . $from . '>' . "\r\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=UTF-8\r\n";

        

        if (!$to) {  // use admin mail if there is no reservation form mail
            $to = get_option('admin_email');
        } else {
            if (strpos($to, ',')) { // if it is comman separated, changed to array
                $to = explode(',', str_replace(' ', '', $to));
            }
        }
        
        if (wp_mail($to, $subject, $content, $headers)) {
            $data['status'] = 'yes';
            echo $body;
        } else {
            $data['status'] = 'no';
            echo $body;
        }
        echo json_encode($data);
        die;
    

}

    add_action('wp_ajax_sendmail', 'ajax_sendmail_func');           // for logged in user  
    add_action('wp_ajax_nopriv_sendmail', 'ajax_sendmail_func');
