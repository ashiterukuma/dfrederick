<?php global $THEME_OPTIONS; ?>
<div class="footer">
	<div class="container">
    	<div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
             	<div class="footer_logo_panel">
                    <?php if($THEME_OPTIONS['footer_logo']): ?>
             		    <img src="<?php echo $THEME_OPTIONS['footer_logo']; ?>" alt="Demo Clothing"/>
                    <?php endif; ?>
             	</div>
                <div class="footer_social_panel">
                    <?php if($THEME_OPTIONS['facebookid']): ?>
                        <a target="__blank" href="<?php echo $THEME_OPTIONS['facebookid'] ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <?php endif; ?>
                    <?php if($THEME_OPTIONS['instagramid']): ?>
               	   		<a target="__blank" href="<?php echo $THEME_OPTIONS['instagramid']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <?php endif; ?>
                    <?php if($THEME_OPTIONS['googleplusid']): ?>
                        <a target="__blank" href="<?php echo $THEME_OPTIONS['googleplusid']; ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <?php endif; ?>
                    <?php if($THEME_OPTIONS['pinterestid']): ?>
                        <a target="__blank" href="<?php echo $THEME_OPTIONS['pinterestid']; ?>"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                    <?php endif; ?>
                </div>
          	</div>
             
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
             	<div class="footer_contact">
                	<h2>Contact us</h2>
                    <?php if($THEME_OPTIONS['info_address']): ?>
                    <p>
	                    <i class="fa fa-map-marker" aria-hidden="true"></i><span><?php echo $THEME_OPTIONS['info_address']; ?></span>
                    </p>
                    <?php endif; ?>
                    <?php if($THEME_OPTIONS['info_phone']): ?>
                    <p>
	                    <i class="fa fa-phone" aria-hidden="true"></i>
	                    <span><?php echo $THEME_OPTIONS['info_phone']; ?></span>
                    </p>
                    <?php endif; ?>
                    <?php if($THEME_OPTIONS['info_email']): ?>
                    <p>
	                    <i class="fa fa-envelope" aria-hidden="true"></i>
	                    <span><a href="mailto:<?php echo $THEME_OPTIONS['info_email']; ?>"><?php echo $THEME_OPTIONS['info_email']; ?></a></span>
                    </p>
                    <?php endif; ?>
                    <?php if($THEME_OPTIONS['info_website']): ?>
                    <p>
	                    <i class="fa fa-globe" aria-hidden="true"></i>
	                    <span><?php echo $THEME_OPTIONS['info_website']; ?></span>
                    </p>
                    <?php endif; ?>
                </div>
            </div>
             
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
				<div class="footer_map"><div id="googleMap" class="footermap" style="width:100%;height:220px;border: 1px dashed #333;"></div></div>
          	</div>
        </div>
    </div>
</div>

<div class="footer_bottom">
	<div class="container"><div class="margintop10" style="display:inline-block;">Copyrights &COPY; <?php echo date('Y'); ?> All Rights Reserved.</div> <span>Developed by <a href="http://www.digitaldots.com.mm" target="_blank"><img src="<?php echo ASSET_URL; ?>images/digitaldots.png"></a></span></div>
</div>

<script>
    $(document).ready(function(){
        var mycenter = new google.maps.LatLng(16.874503, 96.268604);
        function initMap() {
            map = new google.maps.Map(document.getElementById('googleMap'), {
                center: mycenter,
                zoom: 17,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            });

            marker=new google.maps.Marker({
                position: mycenter,
                map: map,
                animation: google.maps.Animation.DROP
            });
            
            marker.setMap(map);
        }
        google.maps.event.addDomListener(window, 'load', initMap);
    });
</script>
<?php wp_footer(); ?>
</body>
</html>