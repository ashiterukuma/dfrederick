<?php global $THEME_OPTIONS; ?>
<!doctype html>
<!--[if lt IE 7 ]>	<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>		<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>		<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>		<html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en"  class="no-js">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<title><?php wp_title(''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<?php if ( file_exists(TEMPLATEPATH .'/favicon.png') ) : ?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png">
<?php endif; ?>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<?php $body_classes = join( ' ', get_body_class() ); ?>
<body class="<?php if( !is_search() )echo $body_classes; ?>">
	
<div class="top_black_panel">
	<div class="container">
    	<div class="row">
    	 	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 top_black_panel_col1"> 
                <?php if($THEME_OPTIONS['info_phone']): ?>
                <i class="fa fa-phone" aria-hidden="true"></i><?php echo $THEME_OPTIONS['info_phone']; ?>
                <?php endif; ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 top_black_panel_col2">  
                <?php if($THEME_OPTIONS['facebookid']): ?>
            	<a target="__blank" href="<?php echo $THEME_OPTIONS['facebookid'] ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <?php endif; ?>
                <?php if($THEME_OPTIONS['instagramid']): ?>
                <a target="__blank" href="<?php echo $THEME_OPTIONS['instagramid']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <?php endif; ?>
                <?php if($THEME_OPTIONS['googleplusid']): ?>
                <a target="__blank" href="<?php echo $THEME_OPTIONS['googleplusid']; ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                <?php endif; ?>
                <?php if($THEME_OPTIONS['pinterestid']): ?>
                <a target="__blank" href="<?php echo $THEME_OPTIONS['pinterestid']; ?>"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                <?php endif; ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 top_black_panel_col3">
                <?php if($THEME_OPTIONS['info_email']): ?>
                <a href="mailto:example@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $THEME_OPTIONS['info_email']; ?> </a>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<div class="main_toppanel">
	<div class="container">
    	<div class="main_toppanel_inner">
        	<div class="row">
            	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 main_toppanel_inner_logo">
                    <a href="<?php echo WP_HOME; ?>"><img src="<?php echo $THEME_OPTIONS['logo']; ?>" alt=""/></a>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 main_toppanel_inner_navbar">
       	            <nav  class="navbar" role="banner">
     					<div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <?php
                            wp_nav_menu(array(
                                'theme_location' => 'main',
                                'menu_class' => 'nav nav-pills',
                                'menu_id' => 'main-menu',
                                'container_class' => 'collapse navbar-collapse menupanel',
                                'container_id' => 'myNavbar',
                                'after' => '<span class="line"></span>',
                            ));
                        ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (is_front_page()): ?>
    
<div class="slider_panel">
    <div class="slider-wrapper theme-default">
        <div id="slider" class="nivoSlider">
            <?php $sliders = get_field('slider',DFRE_PAGE_HOME); ?>
            <?php foreach ($sliders as $slider): ?>
                <img src="<?php echo $slider['url']; ?>" alt="" title="" />
            <?php endforeach ?>
        </div>
    </div>
</div>
<?php endif ?>