<?php get_header(); ?>


<section>
	<?php if ($THEME_OPTIONS['banner']): ?>
	<div class="banner">
		<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	</div>
	<?php endif ?>
</section>

<section class="margintop50 marginbottom50">
	<div class="gallery_panel_heading"><span>Products</span></div>
	<div class="container">
		<div class="row">
			<?php
				$args = array(
					'post_type' => DFRE_TYPE_PRODUCT,
					'posts_per_page' => -1,
					'orderby' => 'rand',
					'post_status' => 'publish',
					);
				$products = get_posts($args);
			?>
			<?php foreach ($products as $product): ?>
            <?php 
            	$brand_taxonomy = wp_get_post_terms($product->ID, DFRE_TERM_PRODUCT, array('parent' => 0)); 
            	$productimg_url = wp_get_attachment_image_src(get_post_thumbnail_id($product->ID), 'full'); 
	            $product_image = aq_resize($productimg_url[0], 255, 291, true, true, true);
            ?>

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="men-pro-item simpleCart_shelfItem" >
						<div class="men-thumb-item">
							<img src="<?php echo $product_image; ?>" class="pro-image-front img-responsive">
							<div class="men-cart-pro">
								<div class="inner-men-cart-pro">
									<a href="<?php echo $productimg_url[0]; ?>" class="link-product-add-cart" rel="prettyPhoto" title="<?php echo $product->post_title; ?>">View</a>
								</div>
							</div>
							<!-- for new product -->
							<span class="product-new-top"><?php echo $brand_taxonomy[0]->name; ?></span>
						</div>
						<div class="item-info-product ">
							<h4><a href="<?php echo $productimg_url[0]; ?>" rel="prettyPhoto" title="<?php echo $product->post_title; ?>"><?php echo $product->post_title; ?></a></h4>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>