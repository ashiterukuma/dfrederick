<?php get_header(); ?>

<div class="slider_top_panel">
	<div class="container">
    	<div class="row">
    		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?php $weclomemsg = get_field('welcome',DFRE_PAGE_HOME); ?>
            	<div class="slider_top_panel_inner"><?php echo $weclomemsg; ?></div>
            </div>
        </div>
    </div>
</div> <!-- end of slider top panel -->

<div class="main_cont_panel">
	<div class="container">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 main_cont_panel_left_cont">
        	<div class="main_cont_panel_left">
                <?php
                    $about = get_post(DFRE_PAGE_ABOUT); 

                    if (strlen($about->post_content)>220) {
                        $excerpt = substr($about->post_content, 0, 220)."...";     
                    }else{
                        $excerpt = $about->post_content;
                    }
                ?>
            	<h2>About <span>Us</span></h2>
            	<?php echo apply_filters('the_content',$excerpt); ?>

                <h2>Our <span>Mission</span></h2>
                <?php 
                    $ourmisson = get_field('our_mission',DFRE_PAGE_ABOUT);
                    if (strlen($ourmisson)>220) {
                        $misson = substr($ourmisson, 0, 220)."...";     
                    }else{
                        $misson = $ourmisson;
                    }
                ?>
                <?php echo apply_filters('the_content',$misson); ?>
                <a href="<?php echo get_permalink($about->ID); ?>">View All</a>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 main_cont_panel_right">
            <?php 
                $aboutimg_url = wp_get_attachment_image_src(get_post_thumbnail_id($about->ID), 'full'); 
                $about_image = aq_resize($aboutimg_url[0], 350, 401, true, true, true);
                
            ?>
        	<img src="<?php echo $about_image; ?>" alt=""/>
    	</div>
    </div>
</div>
<!-- end of main_cont_panel -->

<div class="grey_panel">
	<div class="grey_panel_heading">Infrastructure</div>

    <?php $infrastructures = get_field('infrastructure',DFRE_PAGE_INFRA);$infra_count=1; ?>
        <div class="container">
            <?php 
                foreach ($infrastructures as $infrastructure): 
                if ($infra_count<=4): ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="grey_panel_col">
                            <?php $infra_img = aq_resize($infrastructure['url'],270,276,true,true,true);  ?>
                            <img src="<?php echo $infra_img; ?>" alt=""/>
                            <?php if ($infrastructure['caption']): ?>
                                <p><?php echo $infrastructure['caption']; ?></p>
                            <?php endif ?>
                        </div>
                    </div>
                <?php endif ?>
            <?php $infra_count++; endforeach ?>
            <div class="grey_panel_col_panel"><a href="<?php echo get_permalink(DFRE_PAGE_INFRA); ?>" class="grey_panel_col_panel_link">View All</a></div>
        </div>
</div>
<!-- end of grey panel -->

<div class="icon_panel">
	<div class="container">
        <?php 
            $demo = get_term(3,DFRE_TERM_PRODUCT);
            $demoid = DFRE_TERM_PRODUCT.'_'.$demo->term_id;
            $democategory_img = get_field('category_images',$demoid );
        ?>
    	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 icon_panel_col">
	    	<img src="<?php echo $democategory_img['url']; ?>" alt=""/>
	        <p><?php echo $demo->name; ?></p>
        </div>
        <?php 
            $tiesto = get_term(4,DFRE_TERM_PRODUCT); 
            $tiestoid = DFRE_TERM_PRODUCT.'_'.$tiesto->term_id;
            $tiestocategory_img = get_field('category_images',$tiestoid );
        ?>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 icon_panel_col">
       		<img src="<?php echo $tiestocategory_img['url']; ?>" alt=""/>
         	<p><?php echo $tiesto->name; ?></p>
        </div>
        <?php 
            $export = get_term(5,DFRE_TERM_PRODUCT); 
            $exportid = DFRE_TERM_PRODUCT.'_'.$export->term_id;
            $exportcategory_img = get_field('category_images',$exportid );
        ?>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 icon_panel_col no_border">
       		<img src="<?php echo $exportcategory_img['url']; ?>" alt=""/>
        	<p><?php echo $export->name; ?></p>
        </div>
    </div>
</div>
<?php
    $args = array(
        'post_type' => DFRE_TYPE_PRODUCT,
        'posts_per_page' => 8,
        'post_status' => 'publish',
        );
    $products = get_posts($args);
?>
<div class="grey_panel no_border">
    <div class="grey_panel_heading"><span>Our</span> Products</div>
        <div class="container">
            
        <?php foreach ($products as $product): ?>
        <?php 
            $productimg_url = wp_get_attachment_image_src(get_post_thumbnail_id($product->ID), 'full'); 
            $product_image = aq_resize($productimg_url[0], 270, 330, true, true, true);
        ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="grey_panel_col2">
                    <img src="<?php echo $product_image; ?>" alt=""/>
                    <p><?php echo $product->post_title; ?></p>
                </div>
            </div>
        <?php endforeach ?>
            
        <div class="grey_panel_col_panel"><a href="/product" class="grey_panel_col_panel_link">View All</a></div>
    </div>
</div>


<div class="bottom_logo_panel">
	<div class="container">

        <?php $clientlogos = get_field('gallery', DFRE_PAGE_CLIENT); ?>
        <?php foreach ($clientlogos as $clientlogo): ?>
            <?php $client_img = aq_resize($clientlogo['url'],142,87,true,true,true); ?>
            <a href="<?php echo get_permalink(DFRE_PAGE_CLIENT); ?>"> <img src="<?php echo $client_img; ?>" alt=""/></a>
        <?php endforeach ?>
        
   </div>
</div>

<?php get_footer(); ?>