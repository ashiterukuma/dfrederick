<?php /* Template Name:Client */ ?>
<?php get_header(); ?>

<section>
	<?php if ($THEME_OPTIONS['banner']): ?>
	<div class="banner">
		<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	</div>
	<?php endif ?>
</section>

<section>
	<div class="gallery_panel_heading"><span><?php the_title(); ?></span></div>

	<?php $galleries = get_field('gallery',$post->ID); ?>
	<div class="container">
		<div class="row margintop50 marginbottom50">
			<?php foreach ($galleries as $gallery): ?>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<div class="clientwrap text-center">
					<a href="<?php echo $gallery['url']; ?>" rel="prettyPhoto" title="<?php echo $gallery['title']; ?>">
						<img src="<?php echo $gallery['url']; ?>" class="img-responsive">				
					</a>
					<div class="clienttitle"><?php echo $gallery['title']; ?></div>
				</div>
			</div>
			<?php endforeach ?>
		</div>
	</div>

</section>
<?php get_footer(); ?>