<?php /* Template Name:Infrastructure */ ?>

<?php get_header() ?>

<section>
	<?php if ($THEME_OPTIONS['banner']): ?>
	<div class="banner">
		<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	</div>
	<?php endif ?>
</section>

<section id="pagecontent" class="pagebg">
	<div class="gallery_panel_heading"><span><?php the_title(); ?></span></div>
	<div class="container ">
		<div class="infrwhiteborder">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<?php echo apply_filters('the_content',$post->post_content); ?>
			</div>
		</div>

		<div class="row">
			<ul class="grid cs-style-2">
			<?php $infrastructures = get_field('infrastructure',DFRE_PAGE_INFRA); ?>
			<?php if ($infrastructures): ?>

			<?php foreach ($infrastructures as $infrastructure): ?>
				<?php $infra_img = aq_resize($infrastructure['url'],270,276,true,true,true);  ?>
				<li class="col-xs-12 col-sm-6 col-md-3 col-lg-3 ">
					<a href="<?php echo $infrastructure['url']; ?>" rel="prettyPhoto" title="<?php echo $infrastructure['caption']; ?>">
					<figure >
						<img src="<?php echo $infra_img; ?>" alt="<?php echo $infrastructure['caption']; ?>" class="marginbottom15 margintop15">
						<figcaption>
							<h3><?php echo $infrastructure['caption']; ?></h3>
						</figcaption>
					</figure>
					</a>
				</li>
				<?php endforeach ?>
				<?php endif ?>
			</ul>
		
		</div>
		</div>
	</div>
</section>

<?php get_footer() ?>