<?php /* Template Name:ContactUS */ ?>

<?php get_header() ?>

<section>
	<?php if ($THEME_OPTIONS['banner']): ?>
	<div class="banner">
		<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	</div>
	<?php endif ?>
</section>

<section>
	<div class="gallery_panel_heading"><span><?php the_title(); ?></span></div>
	<div class="container">
		<div class="row margintop50 marginbottom50">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<h3 class="contacttitle">Contact <span class="orangecolor">Form</span></h3>
				<div id="thank_message" style="display: none;" class="thankumsg">
	                <p>"Thank you for choosing to stay with us. we shall reply to you at the soonest."</p>
	            </div> 

	            <form id="enquiry-form" action="<?php the_permalink(); ?>" class="form-horizontal hideform" method="POST"> 
					<div class="form-group">
						<label for="Name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
							<div class="input-group">
    							<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
								<input type="text" class="form-control required" id="Name" placeholder="Your Name" name="name">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="Email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<div class="input-group">
    							<span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
								<input type="email" class="form-control required" id="Email" placeholder="Your Email" name="email">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="Telephone" class="col-sm-2 control-label">Telephone</label>
						<div class="col-sm-10">
							<div class="input-group">
    							<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
								<input type="text" class="form-control" id="Telephone" placeholder="Your Telephone" name="ph">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="Message" class="col-sm-2 control-label">Message</label>
						<div class="col-sm-10">
    						<textarea class="form-control required" id="Message" placeholder="Your Telephone" rows="10" cols="40" name="message"></textarea>
						</div>
					</div>
					<div class="form-group">

						<div class="col-sm-10 col-sm-offset-2">
							<input type="hidden" name="form_type" value="enquirydata">
		                    <input type="submit" class="btncontact" value="Submit" id="sendbtn">
		                    <div id="processing" style="display: none"><p class="load_process">Processing...please wait</p></div>
						</div>
					</div>
				</form>

			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<h3 class="contacttitle">Contact <span class="orangecolor">Address</span></h3>
				<ul class="list-icons list-unstyled">
					<?php if($THEME_OPTIONS['info_address']): ?>
						<li><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $THEME_OPTIONS['info_address']; ?></li>
					<?php endif; ?>
                	<?php if($THEME_OPTIONS['info_phone']): ?>
						<li><i class="fa fa-mobile" aria-hidden="true"></i><?php echo $THEME_OPTIONS['info_phone']; ?></li>
					<?php endif; ?>
					<?php if($THEME_OPTIONS['info_email']): ?>
						<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:<?php echo $THEME_OPTIONS['info_email']; ?>"><?php echo $THEME_OPTIONS['info_email']; ?></a></li>
					<?php endif; ?>
					<?php if($THEME_OPTIONS['info_website']): ?>
						<li><i class="fa fa-globe" aria-hidden="true"></i><?php echo $THEME_OPTIONS['info_website']; ?></li>
					<?php endif; ?>
				</ul>
				<h4 class="socialfollow">Follow <span class="orangecolor">Us</span></h4>
				<ul class="socialwrap">
					<li>
						<?php if($THEME_OPTIONS['facebookid']): ?>
		            	<a target="__blank" href="<?php echo $THEME_OPTIONS['facebookid'] ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		                <?php endif; ?>
	                </li>
	                <li>
		                <?php if($THEME_OPTIONS['instagramid']): ?>
		                <a target="__blank" href="<?php echo $THEME_OPTIONS['instagramid']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
		                <?php endif; ?>
	                </li>
	                <li>
		                <?php if($THEME_OPTIONS['googleplusid']): ?>
		                <a target="__blank" href="<?php echo $THEME_OPTIONS['googleplusid']; ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
		                <?php endif; ?>
	                </li>
	                <li>
		                <?php if($THEME_OPTIONS['pinterestid']): ?>
		                <a target="__blank" href="<?php echo $THEME_OPTIONS['pinterestid']; ?>"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
		                <?php endif; ?>
	                </li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 margintop20">
				<h2 class="maptitle"><i class="iconcolor marginright10 fa fa-map-signs" aria-hidden="true"></i>VIEW ON <span class="orangecolor">MAP</span><i class="iconcolor marginleft10 fa fa-map-signs" aria-hidden="true"></i></h2>
				<div class="margintop20 marginbottom50">
					<div id="googleMap" style="width:100%;height:400px;border: 1px dashed #333;"></div>
				</div>
			</div>
		</div>
	</div>
</section>


<script>
	$(document).ready(function(){
		var mycenter = new google.maps.LatLng(16.874503, 96.268604);
		function initMap() {
			map = new google.maps.Map(document.getElementById('googleMap'), {
				center: mycenter,
				zoom: 17,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			});

			marker=new google.maps.Marker({
	            position: mycenter,
				map: map,
				animation: google.maps.Animation.DROP
			});
			
			var contentString = '<div id="infowindow">'+' <div class="text-center marginbottom20"><img src="<?php echo $THEME_OPTIONS['logo'] ?>" alt="Comet Design & Decoration Logo" /></div>'+'<div id="bodyinfowindow">'+
			'<?php if($THEME_OPTIONS['info_address']): ?><p><i class="fa fa-map-marker"></i> : <?php echo $THEME_OPTIONS['info_address'];?></p><?php endif; ?>'+
			'<?php if($THEME_OPTIONS['info_hotline']): ?><p><i class="fa fa-phone"></i> : <?php echo $THEME_OPTIONS['info_hotline'];?></p><?php endif; ?>'+
			'<?php if($THEME_OPTIONS['info_email']): ?><p><i class="fa fa-envelope"></i> : <?php echo $THEME_OPTIONS['info_email'];?></p><?php endif; ?>'+
			'<?php if($THEME_OPTIONS['info_website']): ?><p><i class="fa fa-globe"></i> : <?php echo $THEME_OPTIONS['info_website'];?></p><?php endif; ?>'+
			'</div>'+'</div>';

			var infowindow = new google.maps.InfoWindow({
          		content: contentString
        	});

			marker.setMap(map);
			marker.addListener('click', function() {
          		infowindow.open(map, marker);
        	});
		}
    	google.maps.event.addDomListener(window, 'load', initMap);
    });
</script>


<?php get_footer() ?>