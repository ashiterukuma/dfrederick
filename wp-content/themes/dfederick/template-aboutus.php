<?php /* Template Name:About Us */ ?>
<?php get_header() ?>


<section>
	<?php if ($THEME_OPTIONS['banner']): ?>
	<div class="banner">
		<img src="<?php echo $THEME_OPTIONS['banner']; ?>">
	</div>
	<?php endif ?>
</section>

<section class="missionbg">
	<div class="gallery_panel_heading"><span><?php the_title(); ?></span></div>
	<div class="container">
		<div class="row">
			<div class="aboutwrap margintop15 marginbottom15">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
					<?php 
			            $aboutimg_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); 
			            // $about_image = aq_resize($aboutimg_url[0], 350, 401, true, true, true);
			            $about_image = $aboutimg_url[0];

			        ?>
			        <?php if ($about_image): ?>
			        <a href="<?php echo $aboutimg_url[0]; ?>" rel="prettyPhoto" title="<?php the_title() ?>">
						<img src="<?php echo $about_image; ?>" class="img-thumbnail">
					</a>
			        <?php endif ?>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<h2 class="missiontitle">Who are <span class="orangecolor">We?</span></h2>
					<?php echo apply_filters('the_content',$post->post_content); ?>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
					<?php $our_misson = get_field('our_mission',$post->ID); ?>
					<h2 class="missiontitle">Our <span class="orangecolor">Mission</span></h2>
					<?php echo apply_filters('the_content',$our_misson); ?>
				</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer() ?>