<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */
 switch ($_SERVER['HTTP_HOST']) {    
    case 'dfrederick.dd':    
        define('DB_NAME', 'dfrederick');
        define('DB_USER', 'root');
        define('DB_PASSWORD', 'nopassword');
        define('DB_HOST', 'localhost');
        break;
    case 'dfrederick.myanmarcafe.info':
        define('DB_NAME', 'dfrederick');
        define('DB_USER', 'root');
        define('DB_PASSWORD', '');
        define('DB_HOST', 'localhost');
        break;
    default :
        define('DB_NAME', '');
        define('DB_USER', '');
        define('DB_PASSWORD', '');
        define('DB_HOST', 'localhost');
        break;
}
$protocol = (!empty($_SERVER['HTTPS']) ) ? 'https://' : 'http://';
define('WP_SITEURL', $protocol . $_SERVER['HTTP_HOST']);
define('WP_HOME', $protocol . $_SERVER['HTTP_HOST']);
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5#hf649%?J:3KPbU!0m`=QlmRt`c4Q+bZ3q_DGLFr5f=sn//g09W3;XF&jz?m.Gn');
define('SECURE_AUTH_KEY',  'Q~+G}eAXEXncj|uZ$o}JpM-lf,5?4#9hVElRZ|rD_;Je:~.S`wk?G6z1h|3PH^kr');
define('LOGGED_IN_KEY',    'a?taY{7{|I!j}s+&qXo,.#^x)u$q+PV;o>,Tu3-e;[9|s(Y/e!)T>/t;-V4+J&eE');
define('NONCE_KEY',        '6;JB^V`sdCs;ScTVS+Wtb0DnOZmrNNY%G-IvD!Ad <DFvgBDyPV<0s,h6|CdlV4o');
define('AUTH_SALT',        'DBm<WQj-i]%WdVzp/Qa;XT]1&8*]fY4@i0}X71[o+@cs:WD+(&j+;J:Zf:`=aY-s');
define('SECURE_AUTH_SALT', '_K[N,@|! CM*&SjX^@k%7j~-D}m4}nY?xTv3v-p{>7m>6B|~2Rp?UAHg~b>7KX$~');
define('LOGGED_IN_SALT',   '=h?GL0yzs$}Q6A%k-9#2nyC#3V!t:fV=xsSHo,llA]= :>>01nT-~a!|eLa)6>un');
define('NONCE_SALT',       'G*@(|/X}q<VaYCZ9X[M:p/R?<^umSq|lJDifw2yx5sV,x>A7]_8~UlWx9b9*/3aY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dfe_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */

define('AUTOSAVE_INTERVAL', 180);  
define( 'WP_AUTO_UPDATE_CORE', false);
define( 'WP_POST_REVISIONS', 3 );
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
